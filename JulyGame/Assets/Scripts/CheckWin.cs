﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CheckWin : MonoBehaviour {
    //FORTESTINGPURPOSES
    public bool isThereAStartCountdownInTheStartOfTheRound;

    public Text winText;
    public bool gameOver;
    public CheckWin otherTrigger;

    public GameObject PlayerRed;
    public GameObject PlayerBlue;

    public GameObject BlueSpawn;
    public GameObject RedSpawn;

    public GameObject Hinge;
    public FloatInWater mapSpawn;

    public Sprite blueDragon;
    public Sprite redDragon;

    public Font font1;

    private int roundNumber = 0;
    private int blueWinAmount;
    private int redWinAmount;

    public bool restarting;
    public float startTimer;
    public float restartTimer;
    public Text countDown;
    public Text replayText;

    public GameObject kongSound;
    public GameObject winSound;

    

    // Use this for initialization
    void Start () {
        gameOver = false;
        otherTrigger.gameOver = false;

        winText.font = font1;
        

        startTimer = 5;
        otherTrigger.startTimer = 5;
        restartTimer = 3.1f;
        otherTrigger.restartTimer = 3.1f;

        if (isThereAStartCountdownInTheStartOfTheRound) {
            startTimer = 0;
            otherTrigger.startTimer = 0;
            restartTimer = 0f;
            otherTrigger.restartTimer = 0f;
            Destroy(replayText);
        }
        
    }

    void RestartGame()
    {
        PlayerBlue.transform.position = BlueSpawn.transform.position;
        PlayerRed.transform.position = RedSpawn.transform.position;

        //clearing velocity from players
        PlayerBlue.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        PlayerRed.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

        


        winText.gameObject.SetActive(false);

        //reset cock
        GameObject.Find("FoodSpawner").GetComponent<FoodSpawnerLogic>().FeederTimer = 7;
        

        PlayerBlue.GetComponent<PlayerController>().läski = 3.875f;
        PlayerRed.GetComponent<PlayerController>().läski = 3.875f;

        PlayerBlue.GetComponent<PlayerController>().fattoMeter.fillAmount =
            (PlayerBlue.GetComponent<PlayerController>().läski - 0.5f) / 4.5f;
        PlayerBlue.GetComponent<PlayerController>().fatText.text =
            Mathf.RoundToInt((PlayerBlue.GetComponent<PlayerController>().läski - 0.5f) / 4.5f * 100).ToString() + "%";

            PlayerRed.GetComponent<PlayerController>().fattoMeter.fillAmount =
            (PlayerRed.GetComponent<PlayerController>().läski - 0.5f) / 4.5f;
        PlayerRed.GetComponent<PlayerController>().fatText.text =
            Mathf.RoundToInt((PlayerRed.GetComponent<PlayerController>().läski - 0.5f) / 4.5f * 100).ToString() + "%";

        //resetting player rotations
        PlayerBlue.GetComponent<PlayerController>().rig.rotation = PlayerBlue.GetComponent<PlayerController>().idleRotation;
        PlayerRed.GetComponent<PlayerController>().rig.rotation = PlayerRed.GetComponent<PlayerController>().idleRotation;

        PlayerBlue.GetComponent<Animator>().SetBool("OnGround", true);
        PlayerBlue.GetComponent<Animator>().SetBool("Walking", false);
        PlayerBlue.GetComponent<Animator>().SetBool("Jump", false);

        PlayerRed.GetComponent<Animator>().SetBool("OnGround", true);
        PlayerRed.GetComponent<Animator>().SetBool("Walking", false);
        PlayerRed.GetComponent<Animator>().SetBool("Jump", false);


        Debug.Log("Game restarted!");

        var clones = GameObject.FindGameObjectsWithTag("food");
        foreach (var food in clones)
        {
            Destroy(food);
        }
        Start();

        startTimer = 3;
        otherTrigger.startTimer = 3;
        restartTimer = 3.1f;
        otherTrigger.restartTimer = 3.1f;

        if (GameObject.Find("Map").CompareTag("WaterMap"))
        {
            mapSpawn.Reset();
        }
    }
    
	
	// Update is called once per frame
	void Update () {
        
        if (gameOver) {

            if (Input.anyKeyDown && (redWinAmount == 3 || blueWinAmount == 3) && restartTimer < 0) {

               
                RestartGame();
                
               
                if (redWinAmount == 3 || blueWinAmount == 3)
                {
                  

                    GameObject.Find("1redpoint").transform.GetChild(0).gameObject.SetActive(false);
                    GameObject.Find("2redpoint").transform.GetChild(0).gameObject.SetActive(false);
                    GameObject.Find("winnerpoint").transform.GetChild(0).gameObject.SetActive(false);

                    GameObject.Find("winnerpoint").transform.GetChild(1).gameObject.SetActive(false);

                    GameObject.Find("1bluepoint").transform.GetChild(0).gameObject.SetActive(false);
                    GameObject.Find("2bluepoint").transform.GetChild(0).gameObject.SetActive(false);
                    blueWinAmount = 0;
                    redWinAmount = 0;
                    roundNumber = 0;
                    otherTrigger.redWinAmount = 0;
                    otherTrigger.blueWinAmount = 0;

                    restartTimer = 3;
                    


                }
                

            }
        }

        startTimer -= Time.deltaTime;

        if (redWinAmount == 3 || blueWinAmount == 3)
        {
            restartTimer -= Time.deltaTime;
        }

        if (restartTimer <= 0)
        {
            replayText.gameObject.SetActive(true);
        }
        else {
            replayText.gameObject.SetActive(false);
        }

            

        if (Mathf.CeilToInt(startTimer) > 0)
        {

            PlayerBlue.GetComponent<PlayerController>().enabled = false;
            PlayerRed.GetComponent<PlayerController>().enabled = false;

        } else if (Mathf.CeilToInt(startTimer) > -2) {
            PlayerBlue.GetComponent<PlayerController>().enabled = true;
            PlayerRed.GetComponent<PlayerController>().enabled = true;
        }
        else {

        }
	}

    public void FixedUpdate()
    {
        if (Mathf.CeilToInt(startTimer) > 0)
        {
            //resetting player rotations
            PlayerBlue.GetComponent<PlayerController>().rig.rotation = PlayerBlue.GetComponent<PlayerController>().idleRotation;
            PlayerRed.GetComponent<PlayerController>().rig.rotation = PlayerRed.GetComponent<PlayerController>().idleRotation;

            PlayerBlue.GetComponent<Animator>().SetBool("OnGround", true);
            PlayerBlue.GetComponent<Animator>().SetBool("Walking", false);
            PlayerBlue.GetComponent<Animator>().SetBool("Jump", false);

            PlayerRed.GetComponent<Animator>().SetBool("OnGround", true);
            PlayerRed.GetComponent<Animator>().SetBool("Walking", false);
            PlayerRed.GetComponent<Animator>().SetBool("Jump", false);

        }

        if (Mathf.CeilToInt(startTimer) >= 4) {
            countDown.gameObject.SetActive(false);
        }

        else if (Mathf.CeilToInt(startTimer) > 0 && Mathf.CeilToInt(startTimer) < 4)
        {
            countDown.gameObject.SetActive(true);
            countDown.text = Mathf.CeilToInt(startTimer).ToString();

            if (startTimer < 0.05f) {
                Hinge.GetComponent<Rigidbody>().AddForce(new Vector3(10, 0, 0), ForceMode.Impulse);

                if (name == "TriggerLeft")
                {
                    Instantiate(kongSound, Hinge.transform.position, new Quaternion());
                }

                startTimer = 0;
            }
        }

        else if (Mathf.CeilToInt(startTimer) > -1)
        {


            countDown.gameObject.SetActive(true);
            countDown.text = "Fight!";

        }
        else
        {
            countDown.text = Mathf.CeilToInt(startTimer).ToString();
            countDown.gameObject.SetActive(false);
            

        }
    }

    private void  StartRound()
    {

       // Time.timeScale = 0;
       


    }

   
    

    

    private void OnTriggerEnter(Collider other)
    {

        


        if (other.gameObject.CompareTag("Blue") && !gameOver)
        {

            Instantiate(winSound, Hinge.transform.position, new Quaternion());

            //SÄÄDÄ TEKSTIN KOKOO!!!!

            winText.gameObject.SetActive(true);
            winText.text = "<color=maroon><size=100>Red</size></color>\nwins the Round!";
            roundNumber++;
            redWinAmount++;
            otherTrigger.redWinAmount++;
            //gives point to red winner
            if (redWinAmount == 1)
            {
                GameObject.Find("2redpoint").transform.GetChild(0).gameObject.SetActive(true);
                
            }
            if (redWinAmount == 2)
            {
                GameObject.Find("1redpoint").transform.GetChild(0).gameObject.SetActive(true);

            }
            if (redWinAmount == 3)
            {
                GameObject.Find("winnerpoint").transform.GetChild(1).gameObject.SetActive(true);
                winText.text = "<color=maroon><size=100>Red</size></color>\nwins the Game!";
            }
            gameOver = true;
            otherTrigger.gameOver = true;
        }
        else if (other.gameObject.CompareTag("Red") && !gameOver)
        {
            Instantiate(winSound, Hinge.transform.position, new Quaternion());


            winText.gameObject.SetActive(true);
            winText.text = "<color=#0D48A3><size=100>Blue</size></color>\nwins the Round!";
            roundNumber++;
            blueWinAmount++;
            otherTrigger.blueWinAmount++;
            //gives point to blue winner
            if (blueWinAmount == 1)
            {
                GameObject.Find("1bluepoint").transform.GetChild(0).gameObject.SetActive(true);

            }
            if (blueWinAmount == 2)
            {
               
                GameObject.Find("2bluepoint").transform.GetChild(0).gameObject.SetActive(true);

            }
            if (blueWinAmount == 3)
            {
                GameObject.Find("winnerpoint").transform.GetChild(0).gameObject.SetActive(true);
                winText.text = "<color=#0D48A3><size=100>Blue</size></color>\nwins the Game!";

            }
            gameOver = true;
            otherTrigger.gameOver = true;
        }

            //automatic round restart
            if ((other.gameObject.CompareTag("Red") || other.gameObject.CompareTag("Blue")) && !restarting
            && ((redWinAmount != 3 && blueWinAmount != 3))) {

            restarting = true;
            StartCoroutine(Restart());
            }
        
    }

    public IEnumerator Restart() {

        

        if (redWinAmount == 3 || blueWinAmount == 3)
        {
            yield return new WaitForEndOfFrame();
            restarting = false;
        }
        else {
            yield return new WaitForSeconds(3);
            RestartGame();
            restarting = false;
        }
    }
}
