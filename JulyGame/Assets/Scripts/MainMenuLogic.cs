﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuLogic : MonoBehaviour
{

    public new Camera camera;

    public Button play;
    public Button quit;
    public Button back;
    public Button stage1;
    public Button stage2;
    public Button stage3;
    public Button stageRandom;

    public GameObject mainScreen;
    public GameObject StageScreen;

    // Use this for initialization
    void Start()
    {
        play.onClick.AddListener(Play);
        quit.onClick.AddListener(Quit);
        back.onClick.AddListener(Back);
        stage1.onClick.AddListener(Stage1);        stage1.onClick.AddListener(Stage1);
        stage2.onClick.AddListener(Stage2);
        stage3.onClick.AddListener(Stage3);
        stageRandom.onClick.AddListener(StageRandom);
        

        //StageScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Play() 
    {
        StageScreen.SetActive(true);
        mainScreen.SetActive(false);

        Debug.Log("Play!");
    }

    public void Quit()
    {
        Application.Quit();
    }
    public void Back()
    {
        StageScreen.SetActive(false);
        mainScreen.SetActive(true);
    }

    public void Stage1()
    {
        SceneManager.LoadScene(1);

    }

    public void Stage2()
    {
        SceneManager.LoadScene(2);

    }

    public void Stage3()
    {
        SceneManager.LoadScene(3);

    }

    public void StageRandom() {
        SceneManager.LoadScene(Mathf.CeilToInt(Random.Range(1, 3)));
    }
}
