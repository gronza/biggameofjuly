﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteSounds : MonoBehaviour {
    
    public Sprite soundsIcon;
    public Sprite muteIcon;

    private void Update()
    {
        if(Input.GetButtonUp("Start1") || Input.GetButtonUp("Start2"))
        {
            if(GameObject.Find("MuteSoundIcon").GetComponent<Image>().sprite == soundsIcon)
            {
                AudioListener.volume = 0;
                GameObject.Find("MuteSoundIcon").GetComponent<Image>().sprite = muteIcon;

            } else
            {
                AudioListener.volume = 1;
                GameObject.Find("MuteSoundIcon").GetComponent<Image>().sprite = soundsIcon;
            }
        }
    }


    public void onClick()
    {
        

        if(AudioListener.volume != 0)
        {
            AudioListener.volume = 0;
            GameObject.Find("MuteSoundIcon").GetComponent<Image>().sprite = muteIcon;

        } else if(AudioListener.volume == 0)
        {
            AudioListener.volume = 1;
            GameObject.Find("MuteSoundIcon").GetComponent<Image>().sprite = soundsIcon;


        }
    }

}
