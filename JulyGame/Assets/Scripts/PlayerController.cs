﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    
    
   

    public float speed;
    public float jumpPower;
    public bool canJump;
    public float jumpTimer;
    public bool isGrounded;
    public float moveLingerTimer;
    public string moveLingerDir;
    public float jumpLag;
    public float jumpHold;
    public bool standingOnPlayer;

    private AudioSource AudioSource;

    public bool ignoreFloor;

    public GameObject enemyPlayer;

    [SerializeField] private AudioClip[] hitSounds;


    [Range(0.5f, 5f)] public float läski;

    [Range(0f, 100f)] public float fatMeter;
    public Text fatText;

    //pilattiin sun koodi Johannes :D::D:D t. Petteri ja Siiri
    public Image fattoMeter;
    
    //melee attack
    public GameObject m_attack;
    public GameObject m_effect;

    public GameObject hitEffect;
    public float damage;
    public float holdTimer;
    public float holdTimer2;
    public bool attackActive;
    public int jabCount;
    public float jabTimer;

    public float hitLagAmount;
    public float attackDuration;
    public float damageDelay;

    public GameObject pickUpEffect;
    public GameObject yumSound;

    //grab related variables
    public bool isGrabbed;
    public GameObject grabEffect;
    public float grabbedTimer;


    //used to restrict player movement while attacking
    public float moveLag;

    public Transform meleeHitPoint;
    public Transform meleeHitPointLeft;
    public Transform meleeHitPointRight;
    public Transform meleeHitPointTop;
    public Transform meleeHitPointDown;

    public float hitPower;

    public float lag;
    public float hitLag;
    public float damageLag;

    //animation control and some rotation stuff
    Animator anim;
    public Transform rig;
    Quaternion lookLeft =  Quaternion.Euler(0, -110, 0);
    Quaternion lookRight =  Quaternion.Euler(0, 120, 0);
    public Quaternion idleRotation = Quaternion.Euler(0, 180, 0);

    //controller support
   

    // Use this for initialization
    void Start()
    {
        

        damageDelay = 0;
        
        canJump = true;
        läski = 3.875f;
        fatMeter = 100f;
        damage = 0;
        attackActive = false;
        jabCount = 0;

        meleeHitPoint = GetComponentInChildren<melee>().gameObject.GetComponent<Transform>();
        meleeHitPointLeft = GetComponentInChildren<meleeLeft>().gameObject.GetComponent<Transform>();
        meleeHitPointRight = GetComponentInChildren<meleeRight>().gameObject.GetComponent<Transform>();
        meleeHitPointTop = GetComponentInChildren<meleeTop>().gameObject.GetComponent<Transform>();
        meleeHitPointDown = GetComponentInChildren<meleeDown>().gameObject.GetComponent<Transform>();

        anim = GetComponent<Animator>();

        AudioSource = GetComponent<AudioSource>();
        

        rig = this.gameObject.transform.GetChild(0).transform;
       
       
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //HIT DIRECTIONS
        //Blue player hit directions
        if (CompareTag("Blue"))
        {
            if (Input.GetKey(KeyCode.D) || Input.GetAxisRaw("HorizontalCon") > 0.2f)
            {
                meleeHitPoint = meleeHitPointRight;
            }
            else if (Input.GetKey(KeyCode.A) || Input.GetAxisRaw("HorizontalCon") < -0.2f)
            {
                meleeHitPoint = meleeHitPointLeft;
            }
            else if (Input.GetKey(KeyCode.W) || Input.GetAxisRaw("VerticalCon") > 0)
            {
                meleeHitPoint = meleeHitPointTop;
            }
            else if (Input.GetKey(KeyCode.S) || Input.GetAxisRaw("VerticalCon") < 0)
            {
                meleeHitPoint = meleeHitPointDown;
            }
        }
        //Red player hit directions
        else if (CompareTag("Red"))
        {
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetAxisRaw("HorizontalCon2") > .2f)
            {
                meleeHitPoint = meleeHitPointRight;
            }
            else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetAxisRaw("HorizontalCon2") < -.2f)
            {
                meleeHitPoint = meleeHitPointLeft;
            }
            else if (Input.GetKey(KeyCode.UpArrow) || Input.GetAxisRaw("VerticalCon2") > 0)
            {
                meleeHitPoint = meleeHitPointTop;
            }
            else if (Input.GetKey(KeyCode.DownArrow) || Input.GetAxisRaw("VerticalCon2") < 0)
            {
                meleeHitPoint = meleeHitPointDown;
            }
        }



        //if the player is grabbed by the other player
        if (isGrabbed)
        {
            //Petteri tekee tän joskus myöhemmin :--D
            //anim.SetBool("Flail", true);

            hitLag = 1;
            grabbedTimer -= Time.deltaTime;

            //putting the player to correct grabbed position
            transform.position = new Vector3(enemyPlayer.transform.position.x,
                enemyPlayer.transform.position.y + 1f,
                enemyPlayer.transform.position.z);



            if (grabbedTimer <= 0)
            {
                isGrabbed = false;

                läski -= 0.5f;
            }
        }

       



        if (hitLag <= 0)
        {
            
                Move();
            

            Melee();

        }
        else
        {
            GetComponent<Rigidbody>().AddForce(new Vector3(0, 60 * läski, 0));

        }

        jumpTimer += Time.deltaTime;
        lag -= Time.deltaTime;
        hitLag -= Time.deltaTime;
        damageLag -= Time.deltaTime;
        jabTimer -= Time.deltaTime;
        moveLag -= Time.deltaTime;
        jumpLag -= Time.deltaTime;
        moveLingerTimer -= Time.deltaTime * 2;

        //adjusting player speed and jump to match fat%
        speed = 3f * (5 / läski);
        if (speed > 15f)
        {
            speed = 15f;
        }

        jumpPower = 20 * (5 / läski);
        if (jumpPower > 42)
        {
            jumpPower = 42;
        }
        if (jumpPower < 30) {
            jumpPower = 30;
        }

        GetComponent<Rigidbody>().mass = läski;

        
        fattoMeter.fillAmount = (läski - 0.5f) / 4.5f;
        fatText.text = Mathf.RoundToInt((läski - 0.5f) / 4.5f * 100).ToString() + "%";

        // HÖHHÖH ::D:D t. Petteri ja Siiri
        /*
        if (CompareTag("Red"))
        {
            fatText.text = "Red Mass:\n" + fatMeter.ToString() + "%";
        }else if (CompareTag("Blue"))
        {
            fatText.text = "Blue Mass:\n" + fatMeter.ToString() + "%";
        }
        */



    }

    private void PlayHitSound()
    {
        
        int n = Random.Range(1, hitSounds.Length);
        AudioSource.clip = hitSounds[n];
        AudioSource.PlayOneShot(AudioSource.clip);

        
        hitSounds[n] = hitSounds[0];
        hitSounds[0] = AudioSource.clip;
    }

    void Move()
    {

        //PLAYER CONTROLS

        anim.SetBool("OnGround", isGrounded);
        

            if ((Input.GetKey(KeyCode.D) && CompareTag("Blue"))
                || (Input.GetKey(KeyCode.RightArrow) && CompareTag("Red"))
                || (Input.GetAxis("HorizontalCon") > 0 &&  CompareTag("Blue"))
                || (Input.GetAxis("HorizontalCon2") > 0 &&  CompareTag("Red")))
            {

                if (moveLag <= 0)
                {
                    moveLingerTimer = 0;
                    transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0), Space.World);
                    anim.SetBool("Walking", true);
                    //this.gameObject.transform.GetChild(0).transform.eulerAngles = new Vector3(0, 125, 0);
                }
                rig.rotation = Quaternion.Lerp(rig.rotation, lookRight, .5f);
                
            }

            else if ((Input.GetKey(KeyCode.A) && CompareTag("Blue"))
                || (Input.GetKey(KeyCode.LeftArrow) && CompareTag("Red"))
                || (Input.GetAxis("HorizontalCon") < 0 &&  CompareTag("Blue"))
                || (Input.GetAxis("HorizontalCon2") < 0 &&   CompareTag("Red")))
            {

               if (moveLag <= 0)
            {
                moveLingerTimer = 0;
                transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0), Space.World);
       
                anim.SetBool("Walking", true);

            //this.gameObject.transform.GetChild(0).transform.eulerAngles = new Vector3(0, -45, 0);
            //rig.rotation = Quaternion.Euler(0, -110, 0);
            }
                rig.rotation = Quaternion.Lerp(rig.rotation, lookLeft, .5f);
            
                //this.gameObject.transform.GetChild(0).transform.eulerAngles = new Vector3(0, -45, 0);

            }


        if (moveLag <= 0)
        {
            if (jumpLag <= 0) { 
                if ((Input.GetKey(KeyCode.Space) && CompareTag("Blue") && canJump)
                || (Input.GetKey(KeyCode.Keypad0) && CompareTag("Red") && canJump)
                || (Input.GetButton("JumpCon") &&  CompareTag("Blue") && canJump)
                || (Input.GetButton("JumpCon2") &&  CompareTag("Red") && canJump))
                {
                        jumpHold += Time.deltaTime;
                }

                if (canJump)
                {
                    if ((Input.GetKeyUp(KeyCode.Space) && CompareTag("Blue"))
                || (Input.GetKeyUp(KeyCode.Keypad0) && CompareTag("Red"))
                || (Input.GetButton("JumpCon") &&  CompareTag("Blue"))
                || (Input.GetButton("JumpCon2") &&  CompareTag("Red"))
                || jumpHold > 0.10f)
                    {



                        //jump animations, starting with crouch
                        anim.SetBool("Jump", true);
                        //Step up animator speed to get a snappier jump

                        // anim.speed = 5;
                       // StartCoroutine(CrouchSpeed());
                       
                    }
                }
            }


            //Fast fall
            if (!isGrounded && jumpLag <= 0)
            {
                if ((Input.GetKey(KeyCode.S) && CompareTag("Blue"))
            || (Input.GetKey(KeyCode.DownArrow) && CompareTag("Red"))
            || (Input.GetAxis("VerticalCon") < -.6f &&   CompareTag("Blue"))
            || (Input.GetAxis("VerticalCon2") < -.6f && CompareTag("Red")))
                {
                    //fastFall = true;
                    holdTimer2 += Time.deltaTime;

                    if (holdTimer2 >= 0.2f)
                    {
                        transform.Translate(new Vector3(0, -0.5f, 0));
                    }
                }
                else {
                    holdTimer2 = 0;
                }
            }


            //this.gameObject.transform.GetChild(0).transform.eulerAngles = new Vector3(0, 125, 0);
            //rig.rotation = Quaternion.Euler(0, 120, 0);

        }
        //lingering movement
        if (Input.GetKey(KeyCode.D) && CompareTag("Blue") 
            || (Input.GetKey(KeyCode.RightArrow) && CompareTag("Red")
            || (Input.GetAxis("HorizontalCon") > 0 &&   CompareTag("Blue"))
            || (Input.GetAxis("HorizontalCon2") > 0 &&  CompareTag("Red"))))
        {
            
            moveLingerTimer = 0.5f;

            if (!isGrounded)
            {
                moveLingerTimer = 1;
            }
            moveLingerDir = "Right";
        }


        //lingering movement
        if (Input.GetKey(KeyCode.A) && CompareTag("Blue")
            || (Input.GetKey(KeyCode.LeftArrow) && CompareTag("Red")
            || (Input.GetAxis("HorizontalCon") < 0 &&   CompareTag("Blue"))
            || (Input.GetAxis("HorizontalCon2") < 0 && CompareTag("Red"))))
        {
            
            moveLingerTimer = 0.5f;

            if (!isGrounded) {
                moveLingerTimer = 1;
            }
            moveLingerDir = "Left";
        }

        if (moveLingerTimer >= 0)
        {
            if (moveLingerDir.Equals("Right"))
            {
                transform.Translate(new Vector3(speed * Time.deltaTime * moveLingerTimer, 0, 0), Space.World);
            }
            else if (moveLingerDir.Equals("Left"))
            {
                transform.Translate(new Vector3(-speed * Time.deltaTime * moveLingerTimer, 0, 0), Space.World);
            }
            
        }

        else
            {
                anim.SetBool("Walking", false);
                
                //rig.rotation = Quaternion.Lerp(rig.rotation, idleRotation, .5f);
            }
        
        if (this.anim.GetCurrentAnimatorStateInfo(0).IsName("Jump") && anim.GetBool("Jump") == true)
        {


            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            if (jumpHold <= 0.10f)
            {
                GetComponent<Rigidbody>().AddForce(new Vector3(0, jumpPower / 1.5f, 0), ForceMode.VelocityChange);
            }
            else if (jumpHold > 0.10f)
            {
                GetComponent<Rigidbody>().AddForce(new Vector3(0, jumpPower, 0), ForceMode.VelocityChange);
            }


            jumpHold = 0;
            canJump = false;
            anim.SetBool("Jump", false);
            jumpLag = 0.2f;
            

            
            
        }
        if (isGrounded)
        {
            anim.ResetTrigger("JumpAttack");
            anim.ResetTrigger("JumpUpAttack");
            anim.ResetTrigger("JumpDownAttack");
            anim.ResetTrigger("Hitstun");

        }
        else
        {
            anim.ResetTrigger("Attack");
            anim.ResetTrigger("Strong");
        }

    }

    IEnumerator CrouchSpeed() {
        anim.speed = 5;
        yield return new WaitForSeconds(0.1f);
        anim.speed = 1;

    }

    void Melee()
    {

        //MELEE HIT INPUT
        //NOW COMPLETE WITH DIRECTIONS
        //check for direction
        
            
        

        //GRAB ATTACK
        if (lag <= 0)
        {
            if ((Input.GetKeyDown(KeyCode.K) && CompareTag("Blue"))
                || (Input.GetKeyDown(KeyCode.Keypad5) && CompareTag("Red")))
            {
                //lingering movement after melee
                if (moveLag <= 0)
                {
                    if ((Input.GetKey(KeyCode.D) && CompareTag("Blue"))
                        || (Input.GetKey(KeyCode.RightArrow) && CompareTag("Red")))
                    {
                        moveLingerTimer = 1;
                        moveLingerDir = "Right";
                    }
                    else if ((Input.GetKey(KeyCode.A) && CompareTag("Blue"))
                        || (Input.GetKey(KeyCode.LeftArrow) && CompareTag("Red")))
                    {
                        moveLingerTimer = 1;
                        moveLingerDir = "Left";
                    }
                }

                //generating the melee attack hitbox in the selected location
                var grabAttack = Instantiate<GameObject>(
                                grabEffect,
                                meleeHitPoint.position,
                                new Quaternion(),
                                meleeHitPoint);
                grabAttack.transform.Rotate(new Vector3(90, 0, 0));

                //assigning the correct tag to the attack hitbox
                if (gameObject.CompareTag("Blue"))
                {
                    grabAttack.tag = "BlueGrab";
                }
                else if (gameObject.CompareTag("Red"))
                {
                    grabAttack.tag = "RedGrab";
                }


                Destroy(grabAttack, attackDuration);

                moveLag = 1.5f;
                lag = 1.5f;
            }
        }


        if (lag <= 0)
        {

            //resets jab combo
            if (jabTimer <= 0)
            {
                jabCount = 0;
            }

            //PLAYER INPUTS A MELEE COMMAND
            if ((Input.GetKeyDown(KeyCode.J) && CompareTag("Blue"))
                || (Input.GetKeyDown(KeyCode.Keypad4) && CompareTag("Red"))
                || (Input.GetButtonDown("AttackCon") &&  CompareTag("Blue"))
                || (Input.GetButtonDown("AttackCon2") &&  CompareTag("Red")))
            {
                holdTimer = 0;
                holdTimer2 = 0;
                
            }


            //Check how long user holds the attack button to determine type of attack
            if ((Input.GetKey(KeyCode.J) && CompareTag("Blue"))
                || (Input.GetKey(KeyCode.Keypad4) && CompareTag("Red"))
                || (Input.GetButton("AttackCon") &&  CompareTag("Blue"))
                || (Input.GetButton("AttackCon2") &&  CompareTag("Red")))
            {
                
                holdTimer += Time.deltaTime;
                holdTimer2 = 0;
                
            }
           
            //Melee attack is initiated, 

            if (holdTimer >= 0.2f)
            {
                
                //lingering movement after melee
                if (moveLag <= 0)
                {
                    if ((Input.GetKey(KeyCode.D) && CompareTag("Blue"))
                        || (Input.GetKey(KeyCode.RightArrow) && CompareTag("Red"))
                        || (Input.GetAxis("HorizontalCon") > .2f &&  CompareTag("Blue"))
                        || (Input.GetAxis("HorizontalCon2") > .2f &&  CompareTag("Red")))
                    {
                        moveLingerTimer = 1;
                        moveLingerDir = "Right";
                    }
                    else if ((Input.GetKey(KeyCode.A) && CompareTag("Blue"))
                        || (Input.GetKey(KeyCode.LeftArrow) && CompareTag("Red"))
                        || (Input.GetAxis("HorizontalCon") < -.2f &&  CompareTag("Blue"))
                        || (Input.GetAxis("HorizontalCon2") < -.2f &&  CompareTag("Red")))
                    {
                        moveLingerTimer = 1;
                        moveLingerDir = "Left";
                    }
                }

                

                if (!isGrounded || standingOnPlayer)
                {
                    if (meleeHitPoint == meleeHitPointLeft || meleeHitPoint == meleeHitPointRight)
                        anim.SetTrigger("JumpAttack");

                    else if (meleeHitPoint == meleeHitPointTop)
                        anim.SetTrigger("JumpUpAttack");

                    else if (meleeHitPoint == meleeHitPointDown)
                        anim.SetTrigger("JumpDownAttack");
                }
                else
                {

                    if (meleeHitPoint == meleeHitPointLeft || meleeHitPoint == meleeHitPointRight)
                        anim.SetTrigger("Strong");

                    else if (meleeHitPoint == meleeHitPointTop)
                        anim.SetTrigger("StrongUp");
                }
                holdTimer = 0;
                attackActive = true;
               
            }



            else if (((Input.GetKeyUp(KeyCode.J) && CompareTag("Blue"))
                || (Input.GetKeyUp(KeyCode.Keypad4) && CompareTag("Red"))
                || (Input.GetButtonUp("AttackCon") &&  CompareTag("Blue"))
                || (Input.GetButtonUp("AttackCon2") &&  CompareTag("Red")))
                && !attackActive)
            {

                 //jabs can only be done in ground
                 if (isGrounded && !attackActive)
                 {
                   
                    if (jabCount >= 0)
                    {

                        //assigning a hitpoint for jab
                        if (meleeHitPoint == meleeHitPointTop || meleeHitPoint == meleeHitPointDown)
                        {
                            if (rig.localRotation.y < 0)
                            {
                                meleeHitPoint = meleeHitPointRight;
                            }
                            else if (rig.localRotation.y >= 0)
                            {
                                meleeHitPoint = meleeHitPointLeft;
                            }
                        }

                        //JAB

                        //lingering movement after melee

                        if (meleeHitPoint == meleeHitPointLeft || meleeHitPoint == meleeHitPointRight)
                        {

                            Debug.Log("Jab! to the" + meleeHitPoint);

                            

                            if (moveLag <= 0)
                            {
                                if (Input.GetKey(KeyCode.D) && CompareTag("Blue")
                                    || (Input.GetKey(KeyCode.RightArrow) && CompareTag("Red")
                                    || (Input.GetAxis("HorizontalCon") > .2f && CompareTag("Blue"))
                                    || (Input.GetAxis("HorizontalCon2") > .2f && CompareTag("Red"))))
                                {
                                    moveLingerTimer = 1;
                                    moveLingerDir = "Right";
                                }
                                else if (Input.GetKey(KeyCode.A) && CompareTag("Blue")
                                    || (Input.GetKey(KeyCode.LeftArrow) && CompareTag("Red")
                                    || (Input.GetAxis("HorizontalCon") < -.2f && CompareTag("Blue"))
                                    || (Input.GetAxis("HorizontalCon2") < -.2f && CompareTag("Red"))))
                                {
                                    moveLingerTimer = 1;
                                    moveLingerDir = "Left";
                                }
                            }

                            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("JumpAttack")
                                && !anim.GetCurrentAnimatorStateInfo(0).IsName("JumpUpAttack")
                                && !anim.GetCurrentAnimatorStateInfo(0).IsName("JumpDownAttack")
                                && !anim.GetCurrentAnimatorStateInfo(0).IsName("Strong")
                                && !anim.GetCurrentAnimatorStateInfo(0).IsName("StrongUp"))
                            {
                                anim.SetTrigger("Attack");
                            }
                        }
                    }
                 }
            }
            attackActive = false;
        }
    }

   


    void StrongAttack() {

        if (lag <= 0)
        {

            //STRONG HOLD MELEE

            //attack damage and throw and stun
            damage = 0.4f;
            hitPower = 35f;
            hitLagAmount = .7f;
            attackDuration = 0.1f;

            lag = .5f;

            moveLag = 0.4f;
            jabCount = 0;




            //generating the melee attack hitbox in the selected location
            var meleeAttack = Instantiate<GameObject>(
                                m_attack,
                                meleeHitPoint.position,
                                new Quaternion(),
                                meleeHitPoint);
            meleeAttack.transform.Rotate(new Vector3(90, 0, 0));

            //hitbox is put to its place
            if (meleeHitPoint == meleeHitPointTop)
            {
                meleeAttack.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            }
            else if (meleeHitPoint == meleeHitPointLeft || meleeHitPoint == meleeHitPointRight)
            {
                meleeAttack.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                meleeAttack.transform.Translate(new Vector3(0, -0.75f, 0), Space.World);
            }
            else if (meleeHitPoint == meleeHitPointDown)
            {
                meleeAttack.transform.localScale = new Vector3(0.5f, 0.5f, 0.3f);
            }

            //assigning the correct tag to the attack hitbox
            if (gameObject.CompareTag("Blue"))
            {
                meleeAttack.tag = "BlueAttack";
            }
            else if (gameObject.CompareTag("Red"))
            {
                meleeAttack.tag = "RedAttack";
            }


            Destroy(meleeAttack, attackDuration);
        }
    }

    void Jab() {

        if (lag <= 0)
        {

            if (rig.localRotation.y < 0)
            {
                meleeHitPoint = meleeHitPointRight;
            }
            else if (rig.localRotation.y >= 0)
            {
                meleeHitPoint = meleeHitPointLeft;
            }


            //attack damage and throw
            damage = 0.15f;
            hitPower = 5f;
            hitLagAmount = .8f;
            attackDuration = 0.2f;

            lag = 0.3f;

            jabCount++;
            jabTimer = 1f;

            //applying lag to the attacking player so you can't move and attack at the same time
            moveLag = 0.4f;

            m_attack.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            if (jabCount > 2)
            {

                lag = 1f;
                jabCount = 0;

            }



            //generating the melee attack hitbox in the selected location
            var meleeAttack = Instantiate<GameObject>(
                            m_attack,
                            meleeHitPoint.position,
                            new Quaternion(),
                            meleeHitPoint);
            meleeAttack.transform.Rotate(new Vector3(90, 0, 0));

            //assigning the correct tag to the attack hitbox
            if (gameObject.CompareTag("Blue"))
            {
                meleeAttack.tag = "BlueAttack";
            }
            else if (gameObject.CompareTag("Red"))
            {
                meleeAttack.tag = "RedAttack";
            }


            Destroy(meleeAttack, attackDuration);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
       
        if (collision.gameObject.CompareTag("Floor")
            || collision.gameObject.CompareTag("Platform")
            || collision.gameObject.CompareTag("Blue")
            || collision.gameObject.CompareTag("Red"))
        {
            isGrounded = true;
            canJump = true;

            //jumpLag = 0.2f;

            //smoke puff effect on hitting the floor
            //var puffPosision = new Vector3(other.transform.position);

            if (collision.gameObject.CompareTag("Floor")
            || collision.gameObject.CompareTag("Platform"))
            {
                var puff = Instantiate(hitEffect,
                                        meleeHitPointDown.transform.position,
                                        new Quaternion());
                puff.transform.Translate(new Vector3(0, 1, 0));
                puff.transform.Rotate(new Vector3(90, 0, 0));

                Destroy(puff, 0.5f);
            }
        }

        if (collision.gameObject.CompareTag("Blue")
            || collision.gameObject.CompareTag("Red")) {
            standingOnPlayer = true;
        }


        //Ignore player collision
        /*
        if (collision.gameObject.CompareTag("Red") || collision.gameObject.CompareTag("Blue"))
        {
            Physics.IgnoreCollision(GetComponent<CapsuleCollider>(),
                collision.gameObject.GetComponent<CapsuleCollider>(), true);
        }
        */

        //EATING FOOD YUM YUM YUM YUM YUM


        if (CompareTag("Red") || CompareTag("Blue"))
        {
            //Tama Konnyaku
            if (collision.gameObject.name.Equals("Tama_Konnyaku(Clone)"))
            {
                Destroy(collision.gameObject);
                if (läski < 5f)
                {
                    läski += 0.3f;
                }

                YumEffect();
            }

            //Dango
            if (collision.gameObject.name.Equals("Dango(Clone)"))
            {
                Destroy(collision.gameObject);

                if (läski < 5f)
                {
                    läski += 0.2f;
                }

                YumEffect();
            }
            
            //Sushi
            if (collision.gameObject.name.Equals("Sushi(Clone)"))
            {
                Destroy(collision.gameObject);

                if (läski < 5f)
                {
                    läski += 0.1f;
                }

                YumEffect();
            }

            //Dumpling
            if (collision.gameObject.name.Equals("Dumpling(Clone)"))
            {
                Destroy(collision.gameObject);

                if (läski < 5f)
                {
                    läski += 0.2f;
                }

                YumEffect();
            }

            //Udon
            if (collision.gameObject.name.Equals("Udon(Clone)"))
            {
                Destroy(collision.gameObject);

                if (läski < 5f)
                {
                    läski += 0.3f;
                }

                YumEffect();
            }

            //Cookie
            if (collision.gameObject.name.Equals("Cookie(Clone)"))
            {
                Destroy(collision.gameObject);

                if (läski < 5f)
                {
                    läski += 0.1f;
                }

                YumEffect();

            }
            // Capataan läskin määrä
            if (läski > 5f)
            {
                läski = 5f;
            }

        }
    }

    public void YumEffect() {
        var yum = Instantiate(pickUpEffect,
                                    meleeHitPointTop.transform.position,
                                    new Quaternion());
        yum.transform.Translate(new Vector3(0, -1, -1));

        Destroy(yum, 0.5f);
        Instantiate(yumSound, meleeHitPointTop.transform.position, new Quaternion());
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor")
            || collision.gameObject.CompareTag("Platform")
            || collision.gameObject.CompareTag("Blue")
            || collision.gameObject.CompareTag("Red"))
        {
            isGrounded = false;
        }

        if (collision.gameObject.CompareTag("Blue")
            || collision.gameObject.CompareTag("Red"))
        {
            standingOnPlayer = false;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor")
            || collision.gameObject.CompareTag("Platform")){
            isGrounded = true;
        }

        if (collision.gameObject.CompareTag("Blue")
            || collision.gameObject.CompareTag("Red"))
        {
            standingOnPlayer = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //check if the other player is also attacking
        StartCoroutine(Example(other));
    }

    private void OnTriggerExit(Collider other) 
    {
        
    }

    IEnumerator Example(Collider other)
    {
        yield return new WaitForEndOfFrame();

        //Applying force and hitlag to the player hit by a melee attack


        if (other != null)
        {
            if ((other.gameObject.CompareTag("BlueAttack") && CompareTag("Red") && damageLag <= 0)
                || (other.gameObject.CompareTag("RedAttack") && CompareTag("Blue") && damageLag <= 0))
            {

                GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

                PlayHitSound();
                var dir = other.gameObject.GetComponentInParent<CapsuleCollider>().transform.position - transform.position;
                dir.x = -dir.x;
                dir.y = -dir.y;
                dir.Normalize();


                GetComponent<Rigidbody>().AddForce(dir * other.gameObject.GetComponentInParent<PlayerController>().hitPower
                    , ForceMode.Impulse);

                //making the sumos fly upwards when hit, for combo attack purposes
                if (other.gameObject.GetComponentInParent<PlayerController>().meleeHitPoint != other.gameObject.GetComponentInParent<PlayerController>().meleeHitPointTop)
                {
                    GetComponent<Rigidbody>().AddForce(transform.up * (15 * (läski))
                        , ForceMode.Impulse);
                }

                //Lowering the body mass of the player hit

                läski -= other.gameObject.GetComponentInParent<PlayerController>().damage;

                if (läski < 0.5f)
                {
                    läski = 0.5f;
                }

                hitLag = other.GetComponentInParent<PlayerController>().hitLagAmount;
                damageLag = 0.3f;

                anim.SetTrigger("Hitstun");
                anim.SetBool("OnGround", false);

                //shake the camera when a player is hit
                CameraShake.Shake(0.05f, (other.gameObject.GetComponentInParent<PlayerController>().hitPower / 100) * (2 / läski));

                //spawn a visual effect when a player is hit
                var meleeAttack = Instantiate<GameObject>(
                                m_effect,
                                other.gameObject.GetComponentInParent<PlayerController>().meleeHitPoint.position,
                                new Quaternion());
                meleeAttack.transform.Rotate(new Vector3(0, 0, 0));
                meleeAttack.transform.localScale = (new Vector3(0.1f, 0.1f, 0.1f));
                meleeAttack.transform.Translate(new Vector3(0, 1, 0), Space.World);

                //hitting player animation is stopped for a brief moment
                //other.gameObject.GetComponentInParent<Animator>().enabled = false;
                StartCoroutine(Pause(other.gameObject.GetComponentInParent<PlayerController>().hitPower));
                Time.timeScale = 0;



                Destroy(meleeAttack, 1f);
            }
        }
    }

    IEnumerator Pause(float power) {
        var time = (power / läski) / 300;
        yield return new WaitForSecondsRealtime(time);
            Time.timeScale = 1;
    }
}