﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FatnessControl : MonoBehaviour {
    public SkinnedMeshRenderer[] smRend;
    PlayerController playerController;
    [Range(0, 100)]
    public float maxfatWeight;
    [Range(0, 100)]
    public float minfatWeight;

    public float läski;

	// Use this for initialization
	void Start () {
        smRend = GetComponentsInChildren<SkinnedMeshRenderer>();
        playerController = GetComponent<PlayerController>();
        Mathf.Clamp(maxfatWeight, 0, 100);
        Mathf.Clamp(minfatWeight, 0, 100);
	}
	
	// Update is called once per frame
	void Update () {
        läski = playerController.läski;
        FatControl();
	}

    void FatControl() {
        foreach (SkinnedMeshRenderer sm in smRend) {
            //float maxfatWeight = sm.GetBlendShapeWeight(0);
            //float minfatWeight = sm.GetBlendShapeWeight(1);

            if (läski >= 2.75f)
            {
                minfatWeight = 0;
                maxfatWeight = (läski - 2.75f) / 2.25f * 100;  
            }
            else {
                maxfatWeight = 0;
                minfatWeight = 100 - ((läski - 0.5f) / 2.25f * 100);
            }


            if (sm.name == "Vaate" || sm.name == "Sumo") {

                sm.SetBlendShapeWeight(0, maxfatWeight);
                sm.SetBlendShapeWeight(1, minfatWeight);

            }

        }
    }
}
