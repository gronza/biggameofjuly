﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorFallThrough : MonoBehaviour {

   


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if (Input.GetKey(KeyCode.S) || (Input.GetAxis("VerticalCon") < 0 ))
        {
            Physics.IgnoreCollision(GetComponent<BoxCollider>(), GameObject.FindGameObjectWithTag("Blue").
                GetComponent<CapsuleCollider>(), true);
        }

        if (Input.GetKey(KeyCode.DownArrow) || (Input.GetAxis("VerticalCon2") < 0 ))
        {
            Physics.IgnoreCollision(GetComponent<BoxCollider>(), GameObject.FindGameObjectWithTag("Red").
                GetComponent<CapsuleCollider>(), true);
        }

        if (Input.GetKeyUp(KeyCode.S) || (Input.GetAxis("VerticalCon") < 0 ))
        {
            StartCoroutine(Blue());
        }

        if (Input.GetKeyUp(KeyCode.DownArrow) || (Input.GetAxis("VerticalCon2") < 0 ))
        { 
            StartCoroutine(Red());
        }

        
    }

    public IEnumerator Blue() {
        yield return new WaitForSeconds(0.15f);

        Physics.IgnoreCollision(GetComponent<BoxCollider>(), GameObject.FindGameObjectWithTag("Blue").
                            GetComponent<CapsuleCollider>(), false);

    }

    public IEnumerator Red() {
        yield return new WaitForSeconds(0.15f);

        Physics.IgnoreCollision(GetComponent<BoxCollider>(), GameObject.FindGameObjectWithTag("Red").
                    GetComponent<CapsuleCollider>(), false);
    }

}
