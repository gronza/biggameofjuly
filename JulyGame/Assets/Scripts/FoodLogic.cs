﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodLogic : MonoBehaviour {

    public bool ignorePlatform;

	// Use this for initialization
	void Start () {
        ignorePlatform = (Random.value > 0.5f);


    }
	
	// Update is called once per frame
	void Update () {

        if (transform.position.z <= 0) {
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if ((collision.gameObject.CompareTag("Platform")) && ignorePlatform) {
            Physics.IgnoreCollision(GetComponent<CapsuleCollider>(), collision.collider);
        }

        else if (collision.gameObject.CompareTag("Floor") || (collision.gameObject.CompareTag("Platform"))) {
            GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
            
        }
    }
}
