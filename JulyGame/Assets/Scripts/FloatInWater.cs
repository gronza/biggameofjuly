﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatInWater : MonoBehaviour
{

    public GameObject spawn;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -1.5)
        {
            GetComponent<Rigidbody>().AddForce(transform.up * 5000, ForceMode.Force);
        }

    }

    public void Reset()
    {
        transform.position = spawn.transform.position;
        transform.rotation = spawn.transform.rotation;
    }
}
