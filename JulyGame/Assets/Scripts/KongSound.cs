﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KongSound : MonoBehaviour {

    AudioSource src;
    public AudioClip clip;
    // Use this for initialization
    void Start()
    {
        src = GetComponent<AudioSource>();

        src.clip = clip;
            src.Play();
        

        Destroy(this.gameObject, 5);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
