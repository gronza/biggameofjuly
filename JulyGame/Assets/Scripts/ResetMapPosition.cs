﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetMapPosition : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Reset()
    {
        GetComponentInParent<FloatInWater>().gameObject.transform.position = transform.position;
        GetComponentInParent<FloatInWater>().gameObject.transform.rotation = transform.rotation;
    }
}
