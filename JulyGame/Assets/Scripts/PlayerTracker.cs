﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTracker : MonoBehaviour {


    public GameObject Player;
    public Transform Indicator;

    Vector3 targetPosOnScreen;
    float ObjectOnCamera;
    int edgeLine;


    



    private void Start()
    {

       

       
    }

    
   


    private void Update()
    {
        targetPosOnScreen = Camera.main.WorldToScreenPoint(Player.transform.position);

        Vector3 center = new Vector3(Screen.width / 2f, Screen.height / 2f, 0);

        float angle = Mathf.Atan2(targetPosOnScreen.y - center.y, targetPosOnScreen.x - center.x) * Mathf.Rad2Deg;


        
        // Debug.Log(targetPosOnScreen);

        if (Screen.width > Screen.height)
            ObjectOnCamera = Screen.width / Screen.height;
        else
            ObjectOnCamera = Screen.height / Screen.width;

        float degreeRange = 360f / (ObjectOnCamera + 1);

        if (angle < 0) angle = angle + 360;

        if (angle < degreeRange / 4f) edgeLine = 0;
        else if (angle < 180 - degreeRange / 4f) edgeLine = 1;
        else if (angle < 180 + degreeRange / 4f) edgeLine = 2;
        else if (angle < 360 - degreeRange / 4f) edgeLine = 3;
        else edgeLine = 0;

        //asetetaan indikaattori oikeaan paikkaan ja kulmaan

        Indicator.position = Camera.main.ScreenToWorldPoint(intersect(edgeLine, center, targetPosOnScreen) + new Vector3(0, -5, 5));

        
        var p_pos = Player.transform.position;
        var i_pos = Indicator.position;
        var i_Angle = Mathf.Atan2(p_pos.y - i_pos.y, p_pos.x - i_pos.x) * 180 / Mathf.PI;
        Indicator.eulerAngles = new Vector3(-i_Angle-180, 90, -90);


        //Pelaajan  1 tarkastelu
        if (onScreen(targetPosOnScreen))
        {
           
            Indicator.GetComponent<MeshRenderer>().enabled = false;
            return;
        } else
        {
           
            DrawIndicator();
        }

       

    }

    public void DrawIndicator()
    {
        transform.LookAt(Player.transform.position);
        Indicator.GetComponent<MeshRenderer>().enabled = true;
        

    }

    //tarkastelee missä suhteessa pelaaja on kameraan ja vetää linja niiden väliin.
    Vector3 intersect(int edgeLine, Vector3 line2point1, Vector3 line2point2)
    {
        float[] A1 = { -Screen.height, 0, Screen.height, 0 };
        float[] B1 = { 0, -Screen.width, 0, Screen.width };
        float[] C1 = { -Screen.width * Screen.height, -Screen.width * Screen.height, 0, 0 };

        float A2 = line2point2.y - line2point1.y;
        float B2 = line2point1.x - line2point2.x;
        float C2 = A2 * line2point1.x + B2 * line2point1.y;

        float det = A1[edgeLine] * B2 - A2 * B1[edgeLine];

        return new Vector3((B2 * C1[edgeLine] - B1[edgeLine] * C2) / det, (A1[edgeLine] * C2 - A2 * C1[edgeLine]) / det, 0);
    }


    bool onScreen(Vector2 input)
    {
        return !(input.x > Screen.width || input.x < 0 || input.y > Screen.height || input.y < 0);




    }

   



    } //end of file
