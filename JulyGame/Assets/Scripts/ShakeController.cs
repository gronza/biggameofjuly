﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeController : MonoBehaviour {

    public float shakeDuration;
    public float shakePower;

    public bool falling;
    public float fallSpeed;

    public PlayerController player;

	// Use this for initialization
	void Start () {
        player = gameObject.GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void LateUpdate () {

        fallSpeed = player.GetComponent<Rigidbody>().velocity.y;

        if (fallSpeed < 0)
        {
            falling = true;
        }
        else {
            falling = false;
        }


	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor") || collision.gameObject.CompareTag("Platform"))
        {

            //screen shake when player hits the floor
            if (falling)
            {
                CameraShake.Shake(0.05f, (player.läski / 1000) * (-(fallSpeed)));
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
    }
}
