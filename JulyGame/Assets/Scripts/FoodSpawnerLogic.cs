﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawnerLogic : MonoBehaviour {


    
    //luodaan Ruokataulukko
    [SerializeField] private Transform[] Foods;

   
    public Transform Spawnpoint;
    //kokin ajastin
    public float FeederTimer = 10;
    //ruuan rigidbody
    private Rigidbody rb;
    //kokin heittoparametrit
    private float thrustY;
    private float thrustX;
    public float Xmin;
    public float Xmax;
    public float Ymin;
    public float Ymax;
    private int ChosenFood;
    public GameObject Kokki;
    Animator Kokkianim;




    //ruuan funktio
    public void SpawnFood()
    {
            var FoodCopy =
            Instantiate(Foods[ChosenFood], Spawnpoint.position, new Quaternion());
            FoodCopy.transform.Rotate(new Vector3(0, 90, 0));

            FoodCopy.GetComponent<Rigidbody>().AddForce(new Vector3(thrustX, thrustY, -0.5f), ForceMode.Impulse);
            FoodCopy.GetComponent<Rigidbody>().angularVelocity = new Vector3(100, 100, 100);
           
           Kokkianim.SetTrigger("ThrowFood");
            
            FeederTimer = 6;
            
    }

    


    // Use this for initialization
    void Start () {

        Kokkianim = Kokki.GetComponent<Animator>();

    }


	
	// Update is called once per frame
	void FixedUpdate () {

        //antaa muuttujille 'thrustx' ja 'thrusty' satunnaiset arvot joka framella.
        thrustX = Random.Range(Xmin, Xmax);
        thrustY = Random.Range(Ymin, Ymax);
        //valitaan ruoka taulukosta
        ChosenFood = Random.Range(0, Foods.Length);

        //ruoka-ajastin
        FeederTimer -= Time.deltaTime;

      

        //jos ruoka ajastin menee nollaan niin spawnataan ruokaa
        if (FeederTimer <= 0)
        {
            SpawnFood();
            
        }

       


    }
}
