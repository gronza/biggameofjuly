﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EatingSound : MonoBehaviour {
    AudioSource src;
    public AudioClip[] clips;
	// Use this for initialization
	void Start () {
        src = GetComponent<AudioSource>();

        if (clips.Length != 0) {
            src.clip = clips[Random.Range(0, clips.Length - 1)];
            src.pitch = Random.Range(0.5f, 0.9f);
            src.Play();
        }

        Destroy(this.gameObject, 2);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
