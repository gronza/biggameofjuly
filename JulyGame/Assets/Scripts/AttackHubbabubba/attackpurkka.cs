﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class attackpurkka : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {

        //check if the other player is also attacking
        if (other.gameObject.CompareTag("BlueAttack") || other.gameObject.CompareTag("RedAttack"))
        {

            if (GetComponentInParent<PlayerController>().damage <= other.GetComponentInParent<PlayerController>().damage)
            {
                Destroy(gameObject);
            }
        }
    }
}
    
