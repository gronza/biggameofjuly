﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorCollision : MonoBehaviour {

    public bool ignoreFloor;

	// Use this for initialization
	void Start () {
        ignoreFloor = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay(Collider other)
    {

    if (other.gameObject.CompareTag("Blue") || other.gameObject.CompareTag("Red"))
        {
            ignoreFloor = true;
            Physics.IgnoreCollision(transform.parent.GetComponent<BoxCollider>(), other, true);
        }
        
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.CompareTag("Blue") || other.gameObject.CompareTag("Red"))
        {
            Physics.IgnoreCollision(transform.parent.GetComponent<BoxCollider>(), other, false);
            ignoreFloor = false;
        }
    }
    
}
