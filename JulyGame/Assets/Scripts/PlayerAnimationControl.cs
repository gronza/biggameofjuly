﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationControl : MonoBehaviour {

    Animator anim;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator> ();
	}

    // Update is called once per frame
    void Update()
    {


        if (Input.GetKey(KeyCode.D))
        {
            anim.SetBool("Walking", true);
        }
        else {
            anim.SetBool("Walking", false);
        }

        if (Input.GetKey(KeyCode.Space)) {
            anim.SetBool("JumpTriggered", true);
        }

    }
}
