﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Transform origPos;
    public Vector3 cameraPos;

    public GameObject player1;
    public GameObject player2;

    public GameObject map;

    public float posX;
    public float posY;
    public float posZ;

    public float speed;

    // Use this for initialization
    void Start()
    {
        //origPos = transform.position;
        speed = 0.05f;

        map = GameObject.Find("Map");
    }

    // Update is called once per frame
    void Update()
    {

        StartCoroutine("Example");
        

        //transform.position = cameraPos;
    }

    private void moveCamera()
    {
        posX = (((player1.transform.position.x + player2.transform.position.x) / 2) + origPos.position.x) / 2;

        if (!map.CompareTag("WaterMap"))
        {
            if (posX <= -115)
            {
                posX = -115;
            }

            if (posX >= -92)
            {
                posX = -92;
            }
        }


        //posY = (((player1.transform.position.y + player2.transform.position.y) / 2) + origPos.y) / 2;
        posY = origPos.position.y;

        posZ = origPos.position.z - Mathf.Abs(player1.transform.position.x - player2.transform.position.x) / 5
            - Mathf.Abs(player1.transform.position.y - player2.transform.position.y) / 5;


        //the camera cant zoom indefinetly
        if (posZ <= -25) {
            posZ = -25;
        }



        cameraPos = new Vector3(posX, posY, posZ);

        speed = Vector3.Distance(transform.position, cameraPos) / 100;

       // Debug.Log(speed);

        transform.position = Vector3.MoveTowards(transform.position, cameraPos, speed);
    }

    IEnumerator Example()
    {
        yield return new WaitForEndOfFrame();
        moveCamera();
    }

}
