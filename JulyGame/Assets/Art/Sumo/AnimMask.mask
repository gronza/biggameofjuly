%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: AnimMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Kraka
    m_Weight: 0
  - m_Path: Sumo
    m_Weight: 0
  - m_Path: SumoRig
    m_Weight: 0
  - m_Path: SumoRig/Root
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip
    m_Weight: 1
  - m_Path: SumoRig/Root/Hip/Chest
    m_Weight: 1
  - m_Path: SumoRig/Root/Hip/Chest/LeftArm_IK
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Chest/LeftArm_IK/LeftArm_IK_end
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Chest/Neck
    m_Weight: 1
  - m_Path: SumoRig/Root/Hip/Chest/Neck/Head
    m_Weight: 1
  - m_Path: SumoRig/Root/Hip/Chest/Neck/Head/Head_end
    m_Weight: 1
  - m_Path: SumoRig/Root/Hip/Chest/RightArm_IK
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Chest/RightArm_IK/RightArm_IK_end
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Chest/Upperarm_Left
    m_Weight: 1
  - m_Path: SumoRig/Root/Hip/Chest/Upperarm_Left/Lowerarm_Left
    m_Weight: 1
  - m_Path: SumoRig/Root/Hip/Chest/Upperarm_Left/Lowerarm_Left/Lowerarm_Left_end
    m_Weight: 1
  - m_Path: SumoRig/Root/Hip/Chest/Upperarm_Right
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Chest/Upperarm_Right/Lowerarm_Right
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Chest/Upperarm_Right/Lowerarm_Right/Lowerarm_Right_end
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/LeftLeg_IK
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/LeftLeg_IK/LeftLeg_IK_end
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Leg_Left
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Leg_Left/Lowerleg_Left
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Leg_Left/Lowerleg_Left/Lowerleg_Left_end
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Lowerleg_Left_null
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Lowerleg_Left_null/Lowerleg_Left_null_end
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Lowerleg_Right_null
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Lowerleg_Right_null/Lowerleg_Right_null_end
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Right_Leg
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Right_Leg/Lowerleg_Right
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/Right_Leg/Lowerleg_Right/Lowerleg_Right_end
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/RightLeg_IK
    m_Weight: 0
  - m_Path: SumoRig/Root/Hip/RightLeg_IK/RightLeg_IK_end
    m_Weight: 0
  - m_Path: Vaate
    m_Weight: 0
